LibreTuner
=========
Open source tuning software for Mazda platforms

[Join the Discord server](https://discord.gg/QQvX2rB)


Screenshots
-----------
![Main and Download window](https://user-images.githubusercontent.com/3116133/37375434-eff414a6-26f5-11e8-9922-91ad76e49e50.png)

Supported vehicles
------------------
* Mazdaspeed6 / Mazda 6 MPS / Mazdaspeed Atenza

Planned support
---------------
* First Generation RX-8


Building
--------
### Requirements
* Qt5
* CMake 2.8.11 or higher

### Build instructions

`git clone https://github.com/Libretuner/LibreTuner.git`

`git submodule update --init --recursive`

`cd LibreTuner`

`cmake .`

`make`

`./LibreTuner`


Credits
-------
Temporary ROM icon made by Freepik from www.flaticon.com 
